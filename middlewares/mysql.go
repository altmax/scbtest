package middlewares

import (
	"log"
	"net/url"
	// Use MySQL driver
	_ "github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"
	"github.com/labstack/echo"
	"github.com/pkg/errors"
)

func Mysql(dsn string) echo.MiddlewareFunc {
	dsn = dsn + "?parseTime=true&loc=" + url.QueryEscape("Europe/Moscow")

	db, err := openDB(dsn)
	if err != nil {
		log.Fatal(err)
	}

	return func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(ctx echo.Context) error {
			ctx.Set("mysql.master", db)

			return next(ctx)
		}
	}
}

func openDB(dsn string) (*sqlx.DB, error) {
	m, err := sqlx.Open("mysql", dsn)
	if err != nil {
		return nil, errors.Wrapf(err, "open mysql on address %s failed", dsn)
	}

	err = m.Ping()
	if err != nil {
		return nil, errors.Wrapf(err, "ping mysql on address %s failed", dsn)
	}

	return m, err
}
