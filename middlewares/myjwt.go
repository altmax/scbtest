package middlewares

import (
	"github.com/labstack/echo"
	"github.com/spf13/viper"
)

func MyJWT(config *viper.Viper) echo.MiddlewareFunc {
	return func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(ctx echo.Context) error {
			ctx.Set("JWTsecret", config.GetString("jwtToken.secret"))
			ctx.Set("JWTexpire", config.GetInt("jwtToken.expires"))
			return next(ctx)
		}
	}
}
