package middlewares

import (
	"bitbucket.org/altmax/scbtest/lib/jwtconf"
	"bitbucket.org/altmax/scbtest/models"
	jwt "github.com/dgrijalva/jwt-go"
	"github.com/jmoiron/sqlx"
	"github.com/labstack/echo"
)

func CheckAuth() echo.MiddlewareFunc {
	return func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(ctx echo.Context) error {
			// fmt.Println("=======Auth=======")
			userCtx := ctx.Get("user").(*jwt.Token)
			claims := userCtx.Claims.(*jwtconf.MyJWT)
			// group := claims.Group
			userTable := models.UserTable{ctx.Get("mysql.master").(*sqlx.DB)}
			id := claims.UserID
			user, err := userTable.FindByID(id)
			if err != nil {
				return echo.ErrUnauthorized
			}
			ctx.Set("user", user)
			return next(ctx)
			// fmt.Println("===", group, "===")
			// return echo.ErrUnauthorized
		}
	}
}
