-- ---
-- Globals
-- ---

-- SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
-- SET FOREIGN_KEY_CHECKS=0;


-- ---
-- Table 'groups'
-- 
-- ---

-- DROP TABLE IF EXISTS `groups`;
		
CREATE TABLE groups (
  id BIGINT NOT NULL AUTO_INCREMENT,
  name VARCHAR(50) NOT NULL,
  rights INT NOT NULL DEFAULT 0,

  PRIMARY KEY (id)
);

-- ---
-- Table 'user'
-- 
-- ---

-- DROP TABLE IF EXISTS user;
		
CREATE TABLE user (
  id BIGINT NOT NULL AUTO_INCREMENT,
  name VARCHAR(50) NOT NULL,
  password VARCHAR(255) NOT NULL,
  group_id BIGINT NOT NULL,

  PRIMARY KEY (id),
  CONSTRAINT FOREIGN KEY (group_id) REFERENCES groups(id)
);

-- ---
-- Table 'note'
-- 
-- ---

-- DROP TABLE IF EXISTS note;
		
CREATE TABLE note (
  id BIGINT NOT NULL AUTO_INCREMENT,
  user_id BIGINT NOT NULL,
  text VARCHAR(1023) NOT NULL,
  dt_created DATETIME NOT NULL,

  PRIMARY KEY (id),
  CONSTRAINT FOREIGN KEY (user_id) REFERENCES user(id)
);


-- ---
-- Test Data
-- ---

INSERT INTO groups (name,rights) VALUES ('admin',15);
INSERT INTO groups (name,rights) VALUES ('user',7);
INSERT INTO user (name,password,group_id) VALUES ('root','root',1);
INSERT INTO user (name,password,group_id) VALUES ('user','user',2);
INSERT INTO note (user_id, text, dt_created) VALUES (2, 'test2', NOW());
INSERT INTO note (user_id, text, dt_created) VALUES (1, 'test3', NOW());
INSERT INTO note (user_id, text, dt_created) VALUES (2, 'test4', NOW());
INSERT INTO note (user_id, text, dt_created) VALUES (2, 'test5', NOW());
INSERT INTO note (user_id, text, dt_created) VALUES (1, 'test6', NOW());
INSERT INTO note (user_id, text, dt_created) VALUES (1, 'test7', NOW());