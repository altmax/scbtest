package run

import (
	"bitbucket.org/altmax/scbtest/server"
	"github.com/spf13/viper"
)

//Run start application with config
func Run(config *viper.Viper) int {
	s := server.New(config)

	listenOn := config.GetString("server.hostname") + ":" + config.GetString("server.port")
	s.Logger.Fatal(s.Start(listenOn))
	return 0
}
