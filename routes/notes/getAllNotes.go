package notes

import (
	"net/http"

	"bitbucket.org/altmax/scbtest/models"
	"github.com/jmoiron/sqlx"
	"github.com/labstack/echo"
)

func GetAll(ctx echo.Context) error {
	user := ctx.Get("user").(*models.User)
	group := user.Perms >> 3
	var notes []models.Note
	var err error
	noteTable := models.NoteTable{ctx.Get("mysql.master").(*sqlx.DB)}
	switch group {
	case 1:
		notes, err = noteTable.FindAll()
		if err != nil {
			return ctx.JSON(http.StatusInternalServerError, err.Error())
		}
		if notes == nil {
			return ctx.JSON(http.StatusOK, "Notebook is empty")
		}
	case 0:
		notes, err = noteTable.FindByUserID(user.ID)
		if err != nil {
			return ctx.JSON(http.StatusInternalServerError, err.Error())
		}
		if notes == nil {
			return ctx.JSON(http.StatusOK, "Notebook is empty")
		}
	}
	return ctx.JSON(http.StatusOK, notes)
}
