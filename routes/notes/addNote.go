package notes

import (
	"net/http"
	"time"

	"bitbucket.org/altmax/scbtest/lib/datetime"
	"bitbucket.org/altmax/scbtest/models"
	"github.com/jmoiron/sqlx"
	"github.com/labstack/echo"
)

func AddNote(ctx echo.Context) error {
	user := ctx.Get("user").(*models.User)
	text := ctx.FormValue("text")
	if text == "" {
		return ctx.JSON(http.StatusBadRequest, "note mustn't be empty")
	}
	noteTable := models.NoteTable{ctx.Get("mysql.master").(*sqlx.DB)}
	var dt datetime.Time
	dt.Scan(time.Now())
	note := models.Note{
		UserID:   user.ID,
		Text:     text,
		Datetime: dt,
	}
	_, err := noteTable.Insert(note)
	if err != nil {
		return ctx.JSON(http.StatusConflict, "failed to create note "+err.Error())
	}
	return ctx.JSON(http.StatusCreated, "note successfully created")
}
