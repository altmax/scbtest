package notes

import (
	"net/http"
	"strconv"

	"bitbucket.org/altmax/scbtest/models"
	"github.com/jmoiron/sqlx"
	"github.com/labstack/echo"
)

func GetNote(ctx echo.Context) error {
	user := ctx.Get("user").(*models.User)
	noteIDS := ctx.Param("id")
	noteID, err := strconv.ParseInt(noteIDS, 10, 64)
	if err != nil {
		return ctx.String(http.StatusBadRequest, "invalid number")
	}
	noteTable := models.NoteTable{ctx.Get("mysql.master").(*sqlx.DB)}
	note, err := noteTable.FindByID(noteID)
	if err != nil {
		return ctx.String(http.StatusBadRequest, "note not found")
	}
	if user.ID != note.UserID && user.Perms>>3 != 1 {
		return echo.ErrForbidden
	}
	return ctx.JSON(
		http.StatusOK,
		map[string]string{
			"note": note.Text,
		},
	)
}
