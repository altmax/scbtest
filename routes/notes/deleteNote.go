package notes

import (
	"strconv"

	"bitbucket.org/altmax/scbtest/models"

	"net/http"

	"fmt"

	"github.com/jmoiron/sqlx"
	"github.com/labstack/echo"
)

func DeleteNote(ctx echo.Context) error {
	user := ctx.Get("user").(*models.User)
	ids := ctx.Param("id")
	fmt.Println("::::::::::", ids)
	noteID, err := strconv.ParseInt(ids, 10, 64)
	if err != nil {
		return ctx.JSON(http.StatusBadRequest, "invalid note number")
	}
	noteTable := models.NoteTable{ctx.Get("mysql.master").(*sqlx.DB)}
	note, err := noteTable.FindByID(noteID)
	if err != nil {
		return echo.ErrNotFound
	}
	if user.ID != note.UserID {
		return echo.ErrForbidden
	}
	err = noteTable.Delete(noteID)
	if err != nil {
		return ctx.JSON(http.StatusConflict, "failed to delete note "+err.Error())
	}
	return ctx.JSON(http.StatusOK, "note successfully deleted")
}
