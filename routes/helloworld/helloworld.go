package helloworldroute

import (
	"net/http"

	"github.com/labstack/echo"
)

//HelloWorld realize ping
func HelloWorld(ctx echo.Context) error {
	cookie, _ := ctx.Cookie("myjwt")
	res := response{
		// Hello: ctx.Request().Header.Get(echo.HeaderAuthorization),
		Hello: cookie.String(),
	}
	return ctx.JSON(http.StatusOK, res)
}

type response struct {
	Hello string `hson:"hello"`
}
