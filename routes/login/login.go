package login

import (
	"net/http"
	"time"

	"bitbucket.org/altmax/scbtest/lib/jwtconf"
	"bitbucket.org/altmax/scbtest/models"
	jwt "github.com/dgrijalva/jwt-go"
	"github.com/jmoiron/sqlx"
	"github.com/labstack/echo"
)

func Login(ctx echo.Context) error {
	username := ctx.FormValue("username")
	pass := ctx.FormValue("password")
	// return ctx.String(http.StatusOK, username+pass)
	db := ctx.Get("mysql.master").(*sqlx.DB)

	userTable := models.UserTable{db}
	user, err := userTable.FindByNamePassword(username, pass)
	if err != nil {
		return ctx.String(http.StatusBadRequest, "invalid username or password "+err.Error())
	}
	group := ""
	switch user.Perms >> 3 {
	case 1:
		group = "admin"
	case 0:
		group = "user"
	}

	exp := ctx.Get("JWTexpire").(int)
	claims := &jwtconf.MyJWT{
		user.Name,
		user.ID,
		group,
		jwt.StandardClaims{
			ExpiresAt: time.Now().Add(time.Second * time.Duration(exp)).Unix(),
		},
	}
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	// Generate encoded token and send it as response.
	t, err := token.SignedString([]byte(ctx.Get("JWTsecret").(string)))
	if err != nil {
		return err
	}
	return ctx.JSON(http.StatusOK, map[string]string{
		"token": t,
	})
}
