package models

import (
	"database/sql"

	"bitbucket.org/altmax/scbtest/lib/datetime"
	"github.com/jmoiron/sqlx"
	"github.com/pkg/errors"
)

type Note struct {
	ID       int64         `db:"id"`
	UserID   int64         `db:"user_id"`
	Text     string        `db:"text"`
	Datetime datetime.Time `db:"dt_created"`
}

type NoteTable struct {
	DB interface {
		sqlx.Execer
		Get(interface{}, string, ...interface{}) error
		Select(interface{}, string, ...interface{}) error
	}
}

// FindByID searches note record by id
func (table *NoteTable) FindByID(id int64) (*Note, error) {
	query := `select id, user_id, text, dt_created 
              from note
             where id = ?;`

	var note Note

	err := table.DB.Get(&note, query, id)
	if err == sql.ErrNoRows {
		return nil, nil
	}
	if err != nil {
		return nil, errors.Wrap(err, "get from db failed")
	}

	return &note, nil
}

// FindByUserID searches note record by user_id
func (table *NoteTable) FindByUserID(userID int64) ([]Note, error) {
	query := `select id, user_id, text, dt_created 
              from note
             where user_id = ?;`

	var notes []Note

	err := table.DB.Select(&notes, query, userID)
	if err == sql.ErrNoRows {
		return nil, nil
	}
	if err != nil {
		return nil, errors.Wrap(err, "get from db failed")
	}

	return notes, nil
}

// FindAll returns all notes
func (table *NoteTable) FindAll() ([]Note, error) {
	query := `select id, user_id, text, dt_created 
              from note;`

	var notes []Note

	err := table.DB.Select(&notes, query)
	if err == sql.ErrNoRows {
		return nil, nil
	}
	if err != nil {
		return nil, errors.Wrap(err, "select from db failed")
	}

	return notes, nil
}

// Insert inserts record to db and sets created ID to note
func (table *NoteTable) Insert(note Note) (int64, error) {
	query := `insert into note (user_id, text, dt_created) values (?, ?, ?);`

	res, err := table.DB.Exec(query, note.UserID, note.Text, note.Datetime.String())
	if err != nil {
		return 0, errors.Wrap(err, "exec failed")
	}

	id, err := res.LastInsertId()
	if err != nil {
		return 0, errors.Wrap(err, "get last insert id failed")
	}

	return id, nil
}

// Exist check if record present in db
func (table *NoteTable) Exist(id int64) (bool, error) {
	query := `select id from note where id = ?;`

	var i int64

	err := table.DB.Get(&i, query, id)
	if err == sql.ErrNoRows {
		return false, nil
	}
	if err != nil {
		return false, errors.Wrap(err, "get from db failed")
	}

	return true, nil
}

// Update updates record in db by ID
func (table *NoteTable) Update(note Note) error {
	exist, err := table.Exist(note.ID)
	if err != nil {
		return errors.Wrap(err, "check for exist failed")
	}
	if !exist {
		return errors.New("record not found")
	}

	query := `update note user_id = ?, text = ?, dt_created = ? where id = ?;`

	_, err = table.DB.Exec(query, note.UserID, note.Text, note.Datetime.String(), note.ID)
	if err != nil {
		return errors.Wrap(err, "exec failed")
	}

	return nil
}

func (table *NoteTable) Delete(id int64) error {
	exist, err := table.Exist(id)
	if err != nil {
		return errors.Wrap(err, "check for exist failed")
	}
	if !exist {
		return errors.New("record not found")
	}
	query := `delete from note where id = ?;`

	_, err = table.DB.Exec(query, id)
	if err != nil {
		return errors.Wrap(err, "exec failed")
	}

	return nil
}
