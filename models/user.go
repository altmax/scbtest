package models

import (
	"database/sql"

	"github.com/jmoiron/sqlx"
	"github.com/pkg/errors"
)

type User struct {
	ID       int64  `db:"id"`
	Name     string `db:"name"`
	Password string `db:"password"`
	Perms    int    `db:"perms"`
}

type UserTable struct {
	DB interface {
		sqlx.Execer
		Get(interface{}, string, ...interface{}) error
		Select(interface{}, string, ...interface{}) error
	}
}

// FindByID searches user record by id
func (table *UserTable) FindByID(id int64) (*User, error) {
	query := `select u.id, u.name, u.password, g.rights as perms 
              from user u join groups g on g.id=u.group_id
             where u.id = ?;`

	var user User

	err := table.DB.Get(&user, query, id)
	if err == sql.ErrNoRows {
		return nil, nil
	}
	if err != nil {
		return nil, errors.Wrap(err, "get from db failed")
	}

	return &user, nil
}

// FindByNamePassword searches user record by name
func (table *UserTable) FindByNamePassword(name, password string) (*User, error) {
	query := `select u.id, u.name, u.password, g.rights as perms 
              from user u join groups g on g.id=u.group_id
             where u.name = ? and u.password = ?;`

	var user User

	err := table.DB.Get(&user, query, name, password)
	if err == sql.ErrNoRows {
		return nil, nil
	}
	if err != nil {
		return nil, errors.Wrap(err, "get from db failed")
	}

	return &user, nil
}

// FindAll returns all users
func (table *UserTable) FindAll() ([]User, error) {
	query := `select u.id, u.name, u.password, g.rights as perms 
              from user u join groups g on g.id=u.group_id;`

	var users []User

	err := table.DB.Select(&users, query)
	if err == sql.ErrNoRows {
		return nil, nil
	}
	if err != nil {
		return nil, errors.Wrap(err, "select from db failed")
	}

	return users, nil
}

// Insert inserts record to db and sets created ID to user
func (table *UserTable) Insert(user User) (int64, error) {
	query := `insert into user (name, password) values (?, ?);`

	res, err := table.DB.Exec(query, user.Name, user.Password)
	if err != nil {
		return 0, errors.Wrap(err, "exec failed")
	}

	id, err := res.LastInsertId()
	if err != nil {
		return 0, errors.Wrap(err, "get last insert id failed")
	}

	return id, nil
}

// Exist check if record present in db
func (table *UserTable) Exist(id int64) (bool, error) {
	query := `select id from user where id = ?;`

	var i int64

	err := table.DB.Get(&i, query, id)
	if err == sql.ErrNoRows {
		return false, nil
	}
	if err != nil {
		return false, errors.Wrap(err, "get from db failed")
	}

	return true, nil
}

// Update updates record in db by ID
func (table *UserTable) Update(user User) error {
	exist, err := table.Exist(user.ID)
	if err != nil {
		return errors.Wrap(err, "check for exist failed")
	}
	if !exist {
		return errors.New("record not found")
	}

	query := `update user name = ?, password = ? where id = ?;`

	_, err = table.DB.Exec(query, user.Name, user.Password, user.ID)
	if err != nil {
		return errors.Wrap(err, "exec failed")
	}

	return nil
}
