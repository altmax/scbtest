package datetime

import (
	"database/sql/driver"
	"encoding/json"
	"fmt"
	"time"
)

const timeFormat = "2006-01-02 15:04:05"

// Time represents mysql's datetime
type Time time.Time

// MarshalJSON implements json.Marshaler
func (t Time) MarshalJSON() ([]byte, error) {
	str := time.Time(t).Format(timeFormat)

	b, err := json.Marshal(str)
	if err != nil {
		return nil, fmt.Errorf("cannot marshal Time to json: %s", err)
	}

	return b, nil
}

// UnmarshalJSON implements unmarshals time
func (t *Time) UnmarshalJSON(b []byte) error {
	var str string

	err := json.Unmarshal(b, &str)
	if err != nil {
		return fmt.Errorf("cannot unmarshal string: %s", err)
	}

	parsed, err := time.ParseInLocation(timeFormat, str, time.Local)
	if err != nil {
		return fmt.Errorf("cannot parse time %s string: %s", str, err)
	}

	*t = Time(parsed)

	return nil
}

// IsZero checks if time is zero
func (t Time) IsZero() bool {
	tt := time.Time(t)

	if tt.Year() == 1 &&
		tt.Month() == time.January &&
		tt.Day() == 1 &&
		tt.Hour() == 0 &&
		tt.Minute() == 0 &&
		tt.Second() == 0 {
		return true
	}

	return false
}

// Value implements driver.Valuer
func (t *Time) Value() (driver.Value, error) {
	if t == nil {
		return nil, nil
	}

	return time.Time(*t), nil
}

// Scan implements sql.Scanner
func (t *Time) Scan(src interface{}) error {
	switch src.(type) {
	case []byte:
		str := string(src.([]byte))

		tt, err := time.ParseInLocation(timeFormat, str, time.Local)
		if err != nil {
			return fmt.Errorf("cannot parse time %s using layout %s", str, timeFormat)
		}

		*t = Time(tt)

	case time.Time:
		*t = Time(src.(time.Time))
		return nil
	default:
		return fmt.Errorf("unsupported type of value %#v", src)
	}

	return nil
}

func (t Time) String() string {
	return time.Time(t).Format(timeFormat)
}
