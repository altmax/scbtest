package datetime

import (
	"database/sql/driver"
	"encoding/json"
	"fmt"
	"time"
)

const dateFormat = "2006-01-02"

// Date represents date
type Date time.Time

// MarshalJSON implements json.Marshaler
func (d Date) MarshalJSON() ([]byte, error) {
	str := time.Time(d).Format(dateFormat)

	b, err := json.Marshal(str)
	if err != nil {
		return nil, fmt.Errorf("cannot marshal Date to json: %s", err)
	}

	return b, nil
}

// UnmarshalJSON implements unmarshals date
func (d *Date) UnmarshalJSON(b []byte) error {
	var str string

	err := json.Unmarshal(b, &str)
	if err != nil {
		return fmt.Errorf("cannot unmarshal string: %s", err)
	}

	parsed, err := time.ParseInLocation(dateFormat, str, time.Local)
	if err != nil {
		return fmt.Errorf("cannot parse time %s string: %s", str, err)
	}

	*d = Date(parsed)

	return nil
}

// Value implements driver.Valuer
func (d *Date) Value() (driver.Value, error) {
	if d == nil {
		return nil, nil
	}

	str := time.Time(*d).Format(dateFormat)

	return str, nil
}

// Scan implements sql.Scanner
func (d *Date) Scan(src interface{}) error {
	switch src.(type) {
	case []byte:
		str := string(src.([]byte))

		dd, err := time.ParseInLocation(dateFormat, str, time.Local)
		if err != nil {
			return fmt.Errorf("cannot parse date %s using layout %s", str, timeFormat)
		}

		*d = Date(dd)
	case string:
		str := src.(string)

		dd, err := time.ParseInLocation(dateFormat, str, time.Local)
		if err != nil {
			return fmt.Errorf("cannot parse date %s using layout %s", str, timeFormat)
		}

		*d = Date(dd)
	case time.Time:
		*d = Date(src.(time.Time))
		return nil
	default:
		return fmt.Errorf("unsupported type of value %#v", src)
	}

	return nil
}

func (d Date) String() string {
	return time.Time(d).Format(dateFormat)
}
