package datetime

import (
	"encoding/json"
	"fmt"
	"time"
)

// Unix represents UNIX timestamp (milleseconds)
type Unix time.Time

// MarshalJSON implements json.Marshaler
func (t Unix) MarshalJSON() ([]byte, error) {
	ms := time.Time(t).UnixNano() / 1000000

	b, err := json.Marshal(ms)
	if err != nil {
		return nil, fmt.Errorf("cannot marshal Time to json: %s", err)
	}

	return b, nil
}

// UnmarshalJSON implements unmarshals time
func (t *Unix) UnmarshalJSON(b []byte) error {
	var ms int64

	err := json.Unmarshal(b, &ms)
	if err != nil {
		return fmt.Errorf("cannot unmarshal milliseconds: %s", err)
	}

	unix := time.Unix(0, ms*1000000)
	if err != nil {
		return fmt.Errorf("cannot parse milliseconds %d: %s", ms, err)
	}

	*t = Unix(unix)

	return nil
}
