package jwtconf

import (
	"errors"

	jwt "github.com/dgrijalva/jwt-go"
)

type MyJWT struct {
	Name   string
	UserID int64
	Group  string
	jwt.StandardClaims
}

func (c MyJWT) Valid() error {
	if err := c.StandardClaims.Valid(); err != nil {
		return err
	}
	if c.UserID == 0 {
		return errors.New("invalid user")
	}
	return nil
}
