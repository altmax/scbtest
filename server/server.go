package server

import (
	"log"
	"net/http"
	"net/http/pprof"

	"bitbucket.org/altmax/scbtest/lib/jwtconf"
	"bitbucket.org/altmax/scbtest/middlewares"
	"bitbucket.org/altmax/scbtest/routes/helloworld"
	"bitbucket.org/altmax/scbtest/routes/login"
	"bitbucket.org/altmax/scbtest/routes/notes"
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	"github.com/spf13/viper"
)

// New instantiates server
func New(config *viper.Viper) *echo.Echo {
	e := echo.New()

	if config.GetString("profile") == "http" {
		log.Println("Profiling enabled")
		e.GET("/debug/pprof/", echo.WrapHandler(http.HandlerFunc(pprof.Index)))
		e.GET("/debug/pprof/heap", echo.WrapHandler(pprof.Handler("heap")))
		e.GET("/debug/pprof/goroutine", echo.WrapHandler(pprof.Handler("goroutine")))
		e.GET("/debug/pprof/block", echo.WrapHandler(pprof.Handler("block")))
		e.GET("/debug/pprof/threadcreate", echo.WrapHandler(pprof.Handler("threadcreate")))
		e.GET("/debug/pprof/cmdline", echo.WrapHandler(http.HandlerFunc(pprof.Cmdline)))
		e.GET("/debug/pprof/profile", echo.WrapHandler(http.HandlerFunc(pprof.Profile)))
		e.GET("/debug/pprof/symbol", echo.WrapHandler(http.HandlerFunc(pprof.Symbol)))
	}
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())
	e.Use(middlewares.Mysql(config.GetString("mysql.master")))

	e.GET("/hello-world", helloworldroute.HelloWorld)

	api := e.Group("/api")
	api.Use(middlewares.MyJWT(config))
	api.POST("/login", login.Login)
	api.GET("/", helloworldroute.HelloWorld)

	notesr := api.Group("/notes")

	jconfig := middleware.JWTConfig{
		Claims:      &jwtconf.MyJWT{},
		SigningKey:  []byte(config.GetString("jwtToken.secret")),
		TokenLookup: config.GetString("jwtToken.lookup"),
	}
	notesr.Use(middleware.JWTWithConfig(jconfig))
	notesr.Use(middlewares.CheckAuth())

	notesr.GET("/:id", notes.GetNote)
	notesr.GET("/", notes.GetAll)
	notesr.GET("/all", notes.GetAll)
	notesr.POST("/add", notes.AddNote)
	notesr.DELETE("/:id", notes.DeleteNote)

	return e
}

// curl -X DELETE  localhost:3000/api/notes/1 --cookie "myjwt=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJOYW1lIjoicm9vdCIsIlVzZXJJRCI6MSwiR3JvdXAiOiJhZG1pbiIsImV4cCI6MTQ5Mzg0Mjg5Mn0.TEH_dEuQyHUNZetM84gcjVvnfLMvIuT3P8PpnjAES9A"

// curl -X POST -d 'text=mytestnote'  localhost:3000/api/notes/add --cookie "myjwt=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJOYW1lIjoicm9vdCIsIlVzZXJJRCI6MSwiR3JvdXAiOiJhZG1pbiIsImV4cCI6MTQ5Mzg0Mjg5Mn0.TEH_dEuQyHUNZetM84gcjVvnfLMvIuT3P8PpnjAES9A"

// curl -X POST -d 'username=root' -d 'password=root'  localhost:3000/api/login

// curl -X POST -d 'username=user' -d 'password=user'  localhost:3000/api/login
