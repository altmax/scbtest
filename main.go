package main

import (
	"os"

	"bitbucket.org/altmax/scbtest/cli"
)

func main() {
	os.Exit(cli.Run())
}
