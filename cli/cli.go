package cli

import (
	"log"
	"os"

	"bitbucket.org/altmax/scbtest/run"

	flag "github.com/spf13/pflag"
	"github.com/spf13/viper"
)

var (
	configPath = flag.String("config", "", "Configuration file path")

	database = flag.String(
		"database",
		"",
		"MySQL address",
	)

	port     = flag.String("port", "3000", "Port to bind to")
	hostname = flag.String("hostname", "localhost", "Hostname to bind to")
)

//Run bind flags to viper config and start application
func Run() int {
	flag.Parse()

	config := viper.New()
	config.SetConfigType("toml")

	if *configPath != "" {
		configFile, err := os.Open(*configPath)
		if err != nil {
			log.Fatalf("opening config failed: %s", err)
			return 1
		}

		err = config.ReadConfig(configFile)
		if err != nil {
			log.Fatalf("reading config failed: %s", err)
			return 2
		}
	}

	config.BindPFlag("mysql.master", flag.Lookup("database"))
	config.BindPFlag("server.host", flag.Lookup("hostname"))
	config.BindPFlag("server.port", flag.Lookup("port"))
	return run.Run(config)
}
